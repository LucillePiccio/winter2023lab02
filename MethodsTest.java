public class MethodsTest{
	public static void main(String[]args){
		//code for void methods
		int x = 5;
		System.out.println(x);
		
		methodNoInputNoReturn();
		System.out.println(x);
		x = x - 5;
		System.out.println(x);
		
		methodOneInputNoReturn(x+10);
		System.out.println(x);
		
		methodTwoInputNoReturn(5, 3.5);
		
		//code for return methods
		int z = methodNoInputReturnInt();
		System.out.println("The returned int value of methodNoInputReturnInt method is: " + z);
		
		double squareRoot = sumSquareRoot(9,5);
		System.out.println("The square root is: " + squareRoot);
		
		String s1 = "java";
		String s2 = "programming";
		System.out.println("The variable s1 is " + s1.length() + " words long");
		System.out.println("The variable s2 is " + s2.length() + " words long");
		
		//code for secondClass class
		System.out.println("The method addOne from another class file gives: " + secondClass.addOne(50));
		/*System.out.println(addTwo(50)); creates an error because in the secondClass file, 
		we omitted the static word when writing our method*/
		
		//the correct way to use a method that omits the word static
		secondClass sc = new secondClass();
		System.out.println("The method addTwo from another class file gives: " + sc.addTwo(50));
	}
	//
	public static void methodNoInputNoReturn(){
		System.out.println("I'm in a method that takes no input and returns nothing");
		int x = 20;
		System.out.println(x);
	}
	//example of method accepting one parameter that returns nothing
	public static void methodOneInputNoReturn(int input){
		System.out.println("Inside the method one input no return");
		System.out.println(input);
	}
	//example of method that accepts two parameters of different types that returns nothing
	public static void methodTwoInputNoReturn(int x, double y){
		System.out.println("x value is: " + x);
		System.out.println("y value is: " + y);
	}
	//example of method that returns a value
	public static int methodNoInputReturnInt(){
		int num = 5;
		return num;
	}
	//method sums the two int and returns the squareRoot of that number
	public static double sumSquareRoot(int num1, int num2){
		double squareRoot = num1 + num2;
		squareRoot = Math.sqrt(squareRoot);
		return squareRoot;
	}
}