import java.util.Scanner;
public class PartThree{
		public static void main(String[]args){
			Scanner reader = new Scanner(System.in);
			//asking the user for numbers
			System.out.println("Please give me a number");
			int num1 = reader.nextInt();
			System.out.println("Please give me another number");
			int num2 = reader.nextInt();
			
			//using the addition method
			double sum = Calculator.add(num1, num2);
			System.out.println("Adding " + num1 + " and " + num2 + " gives: " + sum);
			
			//using substract method
			double diff = Calculator.substract(num1, num2);
			System.out.println("Substracting " + num1 + " and " + num2 + " gives: " + diff);
			
			//using methods that were created from a seperate class file
			//using multiply method
			Calculator cal = new Calculator();
			double product = cal.multiply(num1, num2);
			System.out.println("Multiplying " + num1 + " and " + num2 + " gives: " + product);
			
			//using divide method
			//we already called the Calculator class so no need to do it again here
			double quotient = cal.divide(num1, num2);
			System.out.println("Dividing " + num1 + " and " + num2 + " gives: " + quotient);
		}
}
